protocol Pion {
/*
 Creer_Pion initialise un pion à une certaine position d'un certain camp, et sensei ou non ; le sensei commence au milieu de la première rangée sur l'Arche du côté de son camp.
*/
    func Creer_Pion(x: Int, y: Int, camp: String, sensei: Bool) -> Self

/*
 Est_Sensei renvoie le titre du pion
return : un booléen designant si le pion est sensei ou non avec True si oui False si non
*/
    var Est_Sensei : Bool {get}

/*
 Couleur renvoie le camp d'un pion
return : une chaine de charactère désignant le camp d'un pion "bleu" ou "rouge"
*/
    var Couleur : String {get}

/*
Est_vivant renvoie si le pion ne s'est jamais fait manger par un autre pion
return: True si le pion est vivant False sinon
precondition : le pion a ete cree
*/
    var Est_Vivant : Bool {get set}

/*
 Position renvoie la position d'un pion sur le plateau sous forme d'un couple d'entiers (ligne,colonne)
 precondition : Le Pion est dans le plateau (coordonnées valides)
 postconditoon : Le Pion ne peut être que dans le plateau
*/
    var Position : (Int,Int) {get set}

/*
 Mouvement_Pion Renvoie le pion sur sa nouvelle case et si il a mangé un pion adverse
return : True si il a mangé un pion adverse, False sinon
précondition : Un pion ne peut faire de mouvement si ça le fait sortir du plateau ou si il n'est plus de la partie
postcondition : Le pion est toujours sur le plateau, si True le pion sur la case d'arrivé est supprimé de la partie.
*/
    mutating func Mouvement_Pion(x: Int, y: Int) -> (Self, Bool)

/*
 Affiche_Pion renvoie la position du pion et si il est sensei ou non
 return : un couple de couple d'entier (position) converti en String et un booléen converti en String désignant si il est sensei ou non
 */
    func Affiche_Pion() -> String
    
}







protocol Carte {
/*
 Creer_Carte initialise une carte
return : une carte avec ses Mouvements_Permits et sa couleur (qui permettra de savoir quel joueur commence la partie)
*/
    func Creer_Carte() -> Self

/*
 Appartenance_Carte renvoie le joueur possédant la carte ou "milieu" si elle est au milieu du plateau.
return : une chaine de charactère désignant le camp de la carte donc soit "bleu" soit "rouge" soit "milieu".
precondition : la carte est dans la partie.
*/
    var Appartenance_Carte : String {get set}

/*
 Mouvements_Permits renvoie la liste des positions relatives à une carte, ne prend pas en compte les contraintes du plateau, ex : (1,2) désigne la case 1 au-dessous et 2 à droite ; (-2,-4) 2 au-dessus et 4 à gauche.
return : la liste des positions que la carte permet de réaliser
postcondition : ne prend pas en compte la position d'un pion en particulier, considère que tous les mouvements sont possibles.
*/
    func Mouvements_Permits() -> [(Int, Int)]

/*
 Couleur désigne la couleur de la carte donnée en argument, utilisé pour savoir qui commence la partie.
return : la chaîne de caractère du camp : "bleu" ou "rouge"
*/
    var Couleur : String {get}
}




protocol Partie {
    associatedtype TCarte : Carte
    associatedtype TPion : Pion

/*
 Creer_Partie initialise la partie
return le plateau avec les pions sur la première rangée dont le sensei au milieu et le côté adverse aussi.
Le sensi est donc sur la case Arche.
*/
    func Creer_Partie() -> Self

/*
 Initialiser_Cartes renvoie la liste des cartes de la partie : tirage de 5 parmi les 16 existantes
return : liste des cartes de la partie en cours, avec les deux premières au joueur rouge, les deux suivantes au joueur bleu et la dernière au milieu
postcondition : les cartes tirés sont différentes à chaque tirage
*/
    func Initialiser_Cartes() -> [TCarte]

/*
 Pions_Restants donne la liste des pions encore en jeu dans la partie.
return : la liste des pions d'un camp en particulier en vie dans la partie.
Si le sensei n'en fait pas partie => FinDePartie(Partie)
*/
    func Pions_Restants(camp: String) -> [TPion]
    
/*
 Mouvements_Possibles donne la liste des mouvements possibles pour un pion avec une carte, afin que le joueur devant bouger un pion ait à choisir dans cette liste uniquement des positions valides.
 preconditions : la carte est en possession du joueur (et donc est dans la partie), le pion est de la couleur du joueur et est en vie et a une position valide.
 postcondition : chacune des positions renvoyées est valide : elle est dans le plateau et n'est pas occupé par un pion de même couleur.
 */
    func Mouvements_Possibles(carte: TCarte, pion: TPion) -> [(Int, Int)]

/*
 Deplacement_Possible renvoie en fonction d'une carte et d'un pion si un déplacement est possible
return True si déplacement possible, False sinon
postcondition : si aucun déplacement n'est possible alors le joueur choisi une carte à échanger avec la carte du milieu et passe son tour
*/
    func Deplacement_Possible(carte: TCarte, pion: TPion) -> Bool

/*
 Fin_de_Partie renvoie un booléen, False si les deux sensei sont encore sur le plateau ou si aucun des pions n'a atteint l'Arche adverse
return : True si un des deux sensei n'est plus en vie ou si un sensei est positionné sur l'ancre adverse, False sinon.
*/
    func Fin_De_Partie() -> Bool

/*
 Tour_Suivant passe au tour suivant
return : une chaine de charactère désignant le tour suivant
précondtion : le tour est soit "bleu" ou "rouge"
postcondtion : le tour est l'inverse du tour actuel : Tour_Suivant(Tour_Suivant(partie)) == Tour_Actuel(); le tour est soit "bleu" ou "rouge"
*/
    func Tour_Suivant() -> String

/*
 Tour_Actuel désigne le tour actuel (donc par exemple si le joueur dont c'est le tour arrive sur l'Arche adverse, ce joueur gagne).
return : une chaine de caractère désignant le joueur dont c'est le tour.
postcondition : le tour est soit "bleu" ou "rouge"; le premier tour est la couleur de la carte du milieu
 */
    var Tour_Actuel : String {get set}

/*
 Position_OK renvoie un booléen, True si le couple de coordonnées est dans la liste Mouvements_Possibles et False sinon.
précondition : mouvements correspond à Mouvements_Possibles avec en arguments partie, carte et pion dont on veut tester le mouvement.
return : True si la coordonné est dans la liste mouvements, False sinon
*/
    func Position_OK(mouvements: [(Int,Int)], x:Int, y:Int) -> Bool

/*
Carte_Joueur donne les cartes d'un certain joueur
return : une liste de cartes du camp passé en paramètre
precondition : le camp ne peut être que "rouge", "bleu" ou "milieu"
*/
    func Carte_Joueur(camp: String) -> [TCarte]

/*
Choisir_Carte propose une liste de carte du joueur et lui permet d'en choisir une
precondition: le camp ne peut être que "rouge" ou "bleu"
postcondition: la carte renvoyee fait partie de Carte_Joueur(camp), et la carte est jouable avec les pion du camp, Deplacement_Possible(Carte, Pion)
*/
    func Choisir_Carte(camp: String) -> TCarte

/*
 Echange_Carte permet d'échanger la carte qu'on vient de joué avec la carte du milieu
return une Carte
precondition : la carte en paramètre est celle joué par l'utilisateur
postcondition : la carte retournee est celle du milieu
*/
    func Echange_Carte(carte : TCarte) -> TCarte

/*
 Choisir_Pion renvoie un Pion
 return : un pion
 précondition :la liste_pion_disponible est la liste renvoyé par PionRestant(camp: Tour_Actuel())
                0<=n<liste_pion_disponible.count
 */
    func Choisir_Pion(liste_pion_disponible: [TPion]) -> TPion

/*
DefinirTour definit le Tour_Actuelle du debut comme etant l'appartenance de la carte du milieu
precondition : les cartes ont ete distribuees et la carte passee en parametre est la carte du milieu
postcondition : le tour est definit comme la couleur de la carte du lmilier, ne peut etre que "rouge" ou "bleu"
*/
}
