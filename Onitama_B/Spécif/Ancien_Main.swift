// Types utilisés : Pion, Partie, Carte.
import Pion
import Partie
import Carte

var Partie:Partie=Creer_Partie() //Créé la partie, dont les pions à la bonne place.
var Cartes:[Carte]=Initialiser_Cartes(Partie) //Choisi les 5 cartes du jeu, associées au joueur.
var Pion_Actuel:Pion //Utile dans les tours.
var Carte_Actuelle:Carte //Idem
Definir_Tour(Partie,AppartenanceCarte(Cartes.last))
while !Fin_De_Partie{
    Afficher_Cartes(Partie,Tour_Actuel)
    Carte_Actuelle=Choisir_Carte()                    // A CREER !!!
    Pion_Actuel=Choisir_Pion(Partie,Tour_Actuel)
    x = input("entrez la coordonné en x du déplacement : ")
    y = input("entrez la coordonné en y du déplacement : ")
    while !(isInMouvements(mouvementPossible(p: Partie, pion: Pion_Actuel, carte: Carte_Actuelle), x: x,y: y)){
	x = input("Mauvaise coordoné : entrez la coordonné en x du déplacement : ")
        y = input("Mauvaise coordoné : entrez la coordonné en y du déplacement : ")
    }
    Mouvement_Pion(Partie,Pion_Actuel,x,y)
    Echange_Carte(Carte_Actuelle)
    Tour_Suivant(Partie)
}

// Fin de la partie : annonce du gagnant
if Tour_Suivant(Partie)=="bleu"{
    print("Le joueur gagnant est le joueur bleu !")
}
else { print("Le joueur gagnant est le joueur rouge !")
}
