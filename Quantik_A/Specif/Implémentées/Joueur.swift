import Piece

struct Joueur{
    //sacPiece: Joueur -> [Piece]
    //donne la liste des pièces que possède le joueurs
    //var sacPiece:[TPiece] {get}

    //couleur: Joueur -> Couleur
    //donne la couleur du joueur conformement au type Couleur

    var couleur : Couleur {return couleur}
    
    //init :  Couleur -> Joueur
    //initialise un joueur qui dispose de 8 pièces de même couleur que la sienne
    //les pieces sont les suivantes: 2 cylindres, 2 pyramides, 2 sphères, 2 cubes
    //Pre: La couleur du joueur (et donc de ses pièces) est soit noir ou soit blanc
    //Post: couleur(joueur)==couleur(piece(joueur))
    //Post: Taille du sacPiece = 8
    //Post: type(SacPiece[0])=type(SacPiece[1]) = cylindre
    //Post: type(SacPiece[2])=type(SacPiece[3]) = pyramide
    //Post: type(SacPiece[4])=type(SacPiece[5]) = sphère
    //Post: type(SacPiece[6])=type(SacPiece[7]) = cube

    private var piece_sac : [Piece] // contient les piece par encore posée

    init(_ c: Couleur){
        self.couleur = c
        var piece1 : Piece = Piece(c, "cylindre")
        var piece2 : Piece = Piece(c, "cylindre")
        var piece3 : Piece = Piece(c, "cône")
        var piece4 : Piece = Piece(c, "cône")
        var piece5 : Piece = Piece(c, "sphère")
        var piece6 : Piece = Piece(c, "sphère")
        var piece7 : Piece = Piece(c, "cube")
        var piece8 : Piece = Piece(c, "cube")
        self.piece_sac = [piece1,piece2,piece3,piece4,piece5,piece6,piece6,piece7,piece8]
    }
    
    //pieceRestantes: Joueur -> Int
    //donne le type et la position dans le sac de chaque pieces restantes au joueur (i.e. celles qui ne sont pas posées)
    func piece_Restantes() -> [Piece]{
        var piece_restante : [Piece]
        var k : Int = 0
        for i in 0..<8{
            if(piece_sac[i].est_posee()){
                piece_restante[k] = piece_sac[i]
                k += 1
            }
        }
    }
}
