public enum Couleur : String {
    case noir="Noir"
    case blanc="Blanc"
}

public enum typePiece : String {
    case sp="sphère"
    case cy="cylindre"
    case cu="cube"
    case co="cône"
}

class Location{
    public fileprivate(set) var x : Int
    public fileprivate(set) var y : Int
    var occupee : Bool // à chaque fois qu'on pose une piece il faut qu'on set cette var à True et il faut regarder si cette var est à False
    var piece : Piece?
    
    init(_ x: Int, _ y: Int){
        self.x = x
        self.y = y
        self.occupee = false
        self.piece = nil
    }

    func est_occupee()->Bool{
        return self.occupee
    }

    func est_occupee_par()->Piece?{
        if self.est_occupee(){
            return self.piece
        }else{
            return nil
        }
    }
}

class Piece{

    //couleur: Piece -> Couleur
    //stocke la couleur de la pièce (soit noir soit blanc)
    //Pre: La pièce a été initialisée
    public private(set) var couleur: Couleur
    
    //type: Piece -> TypePiece
    //stocke le type de la pièce (conformement au type typePiece)
    public private(set) var type: typePiece

    //coord: Piece -> Location | Vide
    //stocke les coordonnees de la piece si elle est placée sinon Vide
    public var coord:Location?

    // Numero qui sert à différencier les deux pièces identiques que possède chaque joueur
    public var num:Int
   
    
    //init Couleur x typePiece -> Piece
    //initialise une piece de couleur et de type donné dont les coordonnées sont Vide (i.e. la pièce n'est pas placée)
    //Pre: La couleur de la pièce est Noir ou Blanc (conformement au type Couleur)
    //Pre: Le type de la pièce est Cube ou Cylindre ou Sphere ou Pyramide (conformement au type typePiece)
    //Post: coord(init())=Vide
    init(_ c:Couleur,_ t:typePiece,_ n:Int) {
        self.couleur=c
        self.type=t
        self.num=n
    }

    //est_posee: Piece -> Bool
    //retourne si la pièce est posée, c'est à dire que les coordonnées ne sont pas vides
    //Pre: La pièce a été créee
    fileprivate func est_posee() -> Bool {
        return self.coord != nil
    }
    
    //est_posee_sur: Piece -> Location | Vide
    //retourne sur quelle case la piece est posée, si elle n'est pas posée alors retourne Vide
    //Pre: La pièce a été créee
    //Post: La case retournée existe
    fileprivate func est_posee_sur()->Location? {
        if (self.est_posee()){
            return self.coord
        }else {return nil}   
    } 
}

class Joueur{
    //couleur: Joueur -> Couleur
    //donne la couleur du joueur conformement au type Couleur
    public private(set) var couleur : Couleur

    
    //init :  Couleur -> Joueur
    //initialise un joueur qui dispose de 8 pièces de même couleur que la sienne
    //les pieces sont les suivantes: 2 cylindres, 2 pyramides, 2 sphères, 2 cubes
    //Pre: La couleur du joueur (et donc de ses pièces) est soit noir ou soit blanc
    //Post: couleur(joueur)==couleur(piece(joueur))
    //Post: Taille du sacPiece = 8
    //Post: type(SacPiece[0])=type(SacPiece[1]) = cylindre
    //Post: type(SacPiece[2])=type(SacPiece[3]) = pyramide
    //Post: type(SacPiece[4])=type(SacPiece[5]) = sphère
    //Post: type(SacPiece[6])=type(SacPiece[7]) = cube

    public var piece_sac : [Piece] // contient les piece par encore posée

    init(_ c: Couleur){
        self.couleur = c
        let cy: typePiece = typePiece.cy
        let cu: typePiece = typePiece.cu
        let sp: typePiece = typePiece.sp
        let co: typePiece = typePiece.co
        
        let piece1 : Piece = Piece(c, cy, 1)
        let piece2 : Piece = Piece(c, cy, 2)
        let piece3 : Piece = Piece(c, co, 3)
        let piece4 : Piece = Piece(c, co, 4)
        let piece5 : Piece = Piece(c, sp, 5)
        let piece6 : Piece = Piece(c, sp, 6)
        let piece7 : Piece = Piece(c, cu, 7)
        let piece8 : Piece = Piece(c, cu, 8)

        self.piece_sac = [piece1,piece2,piece3,piece4,piece5,piece6,piece7,piece8]
    }
    
    //pieceRestantes: Joueur -> String
    //donne le type et la position dans le sac de chaque pieces restantes au joueur (i.e. celles qui ne sont pas posées)
    func piece_Restantes() -> String{
        var piece_restante : String = ""
        for i in 0...7{
            if(!self.piece_sac[i].est_posee()){
                piece_restante += String(i) + " { "    //Affiche la position dans le sac des pièces
                
                var typeStrUni : String = ""
                
                if(self.piece_sac[i].couleur.rawValue == "Noir"){
                    if(self.piece_sac[i].type.rawValue=="sphère"){typeStrUni = "○" //sphere 

                    }else if(self.piece_sac[i].type.rawValue=="cube"){typeStrUni = "□"//carre noir
    
                    }else if(self.piece_sac[i].type.rawValue=="cylindre"){typeStrUni = "▯"//cylindre

                    }else if(self.piece_sac[i].type.rawValue=="cône"){typeStrUni = "△"//cone
                        }
                }else if(self.piece_sac[i].couleur.rawValue == "Blanc"){
                    if(self.piece_sac[i].type.rawValue=="sphère"){typeStrUni = "●" //sphere

                    }else if(self.piece_sac[i].type.rawValue=="cube"){typeStrUni = "■" //cube

                    }else if(self.piece_sac[i].type.rawValue=="cylindre"){typeStrUni = "▮" //cylindre

                    }else if(self.piece_sac[i].type.rawValue=="cône"){typeStrUni = "▲" //cone
                        }
                }
                
                piece_restante += "type : " + typeStrUni
                piece_restante += " }\n"
            }
        }
        return piece_restante
    }
}

class Plateau{

     //init : -> Plateau
    //initialise le plateau de jeu (un plateau de jeu de 4 cases sur 4 cases)
    //crée deux joueurs de couleurs différentes (noir ou blanc)
    //chaque joueur possède 8 pièces de sa couleur (2 Cylindres, 2 Cubes, 2 Sphères, 2 Pyramides)
    //Post: les cases du plateau ne sont pas occupées à la création
    //Post: partie_en_cours(init())=true
    init(){
        let ligne : [Location] = []
        
        for i in 0...3{
            if i>0{
                self.grille.append(ligne)}
            for j in 0...3{
                self.grille[i].append(Location(i,j))
            }
        }
        let random : Int = Int.random(in: 0...1)
        let blanc = Couleur.blanc
        let noir = Couleur.noir
        
        if (random == 1){
            self.joueurs.append(Joueur(noir))
            self.joueurs.append(Joueur(blanc))
            }else{self.joueurs.append(Joueur(blanc))
                  self.joueurs.append(Joueur(noir))
            }

        self.partie_en_cours = true
    }

    var grille: [[Location]] = [[]]
    
    //partie_en_cours: Plateau -> Bool
    //stocke si la partie est en cours, dans le cas contraire un joueur a remporter la partie
    //Lorsque qu'on crée la partie, la partie est en cours
    //Pre: La partie doit avoir été créee
    var partie_en_cours: Bool

    //joueurs: Plateau -> [Joueur]
    //renvoie la liste des joueurs
    //Pre: La partie doit avoir été créee
    fileprivate(set) var joueurs: [Joueur] = []

    //peut_jouer: Plateau x Joueur -> Bool
    //le joueur peut-il jouer c'est à dire avoir au moins une possibilité pour poser une de ses pièces restantes, si oui la partie continue, sinon l'autre joueur gagne
    //Pre: Le plateau de jeu a été initialisé
    //Pre: La partie est en cours
    //Pre: Le joueur a été initialisé
    func peut_jouer(_ j: Joueur) -> Bool{
        var peut_jouer: Bool = false
        var i: Int = 0
        var x: Int = 0
        var y: Int = 0
        let loc: Location = Location(x,y)
        if (partie_en_cours){
            while i<8 && !peut_jouer{
                while x<4 && !peut_jouer{
                    while y<4 && !peut_jouer{
                        peut_jouer = coup_autorise(j, loc, j.piece_sac[i])
                        y += 1
                        loc.y = y
                    }
                    x += 1
                    loc.x = x
                }
                i += 1
            }
            return i<=8 // si on a été au bout on n'a pas trouvé de coup jouable
        }else{return false}
    }
        
    //coup_autorise: Plateau x Joueur x Location -> Bool /!\ ajout du paramètre piece désignant la piece jouée
    //renvoie si le coup tente par le joueur est reglementaire
    func coup_autorise(_ j: Joueur,_ loc: Location, _ p: Piece) -> Bool {                                                 
        var listePiecesNum: [Int] = []
        for k in 0...j.piece_sac.count-1 {  // On récupère les numéros attribués aux pièces encore jouables
            if !j.piece_sac[k].est_posee(){
                listePiecesNum.append(j.piece_sac[k].num)
            }
        }
        return (!self.colonne_occupee(p, loc.y, j) && !self.ligne_occupee(p, loc.x, j) && !self.region_occupee(p, loc, j) && listePiecesNum.contains(p.num))
    }
    
    // fonction qui est utile aux 3 suivantes
    private func Meme_Piece_Adverse(_ c : Location,_ j : Joueur,_ p : Piece) -> Bool {
        return (c.est_occupee_par()!.couleur != j.couleur && c.est_occupee_par()!.type == p.type)
        // On peut forcer l'execution car on vérifie avant dans les programmes si la case est occupée
        // Retourne si une piece adverse de meme type est presente sur la case
    }
    

    //colonne_occupee: Plateau x Piece x Int -> Bool /!\ ajout du paramètre joueur désignant le joueur actuelle
    //renvoie "true" si la piece peut être placée dans la colonne
    //Pre: "c" est compris entre 0 et 3
    //Pre: La piece "p" a été creee mais n'a pas déjà été posee
    func colonne_occupee(_ p: Piece,_ c:Int, _ JoueurCourant: Joueur) -> Bool {
        //Pas besoin de checker si la case est occupee ou non
        // besoin de vérifier si la piece p a été posé ou non
        var Case_Actuelle : Location //Variable de lisibilite
        var PasPossible: Bool = false

        if (!p.est_posee()){
            for k in 0...3{
            //On regarde les 4 lignes correspondant a la colonne c
                Case_Actuelle=self.grille[k][c]
                if (Case_Actuelle.est_occupee()) {
                    if Meme_Piece_Adverse(Case_Actuelle, JoueurCourant, p) {
                        PasPossible = true
                    }
                }
            }
        }
        return PasPossible
    }
            
    //ligne_occupee: Plateau x Piece x Int -> Bool  /!\ ajout du paramètre joueur désignant le joueur actuelle
    //renvoie "true" si la pièce peut être placée dans la ligne
    //Pre: "l" est compris entre 0 et 3
    //Pre: La pièce "p" a été créee mais n'a pas déjà été posée
    func ligne_occupee(_ p: Piece,_ l: Int, _ JoueurCourant: Joueur) -> Bool {
        var Case_Actuelle: Location //Variable de lisibilite
        var PasPossible: Bool = false

        for k in 0...3{
            //On regarde les 4 colonnes correspondant a la ligne l
            Case_Actuelle=self.grille[l][k]

            if  Case_Actuelle.est_occupee() {
                if Meme_Piece_Adverse(Case_Actuelle, JoueurCourant, p) {
                    PasPossible = true
                }
            }
        }
        return PasPossible
    }

    //region_occupee: Plateau x Piece x Location -> Bool  /!\ ajout du paramètre joueur désignant le joueur actuelle
    //renvoie "true" si la piece peut être posée dans la région
    //Pre: c.x compris entre 0 et 3 et c.y compris entre 0 et 3
    //Pre: La case saisie est initialisée et dans le plateau
    //Pre: La pièce "p" a été créee mais n'a pas déjà été posée
    func region_occupee(_ p: Piece,_ c: Location, _ JoueurCourant: Joueur) -> Bool {
        var Case_Actuelle: Location //Indice
        var PasPossible: Bool = false

        for l in (c.x/2)*2...(c.x/2)*2+1{
            // On regarde les 2 lignes correspondant a la region
            for c in (c.y/2)*2...(c.y/2)*2+1 { // Idem avec les colonnes
                Case_Actuelle=self.grille[l][c]
                if  Case_Actuelle.est_occupee() {
                // Pour chacune de ces cases, on regarde si la case correspondante est occupee par une piece
                    if Meme_Piece_Adverse(Case_Actuelle, JoueurCourant, p) {
                        PasPossible = true
                    }
                }
            }
        }
        return PasPossible
    }
    
    //ligne_complete: Plateau x Location -> Bool
    //renvoie si la ligne correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau

    func ligne_complete(_ c: Location) -> Bool {                 //UNE LIGNE PEU ETRE PLEINE MAIS PAS COMPLETE (si un joueur pose 2 fois la meme piece)
        var TypeDifferents: [typePiece] = []
        var Case_Actuelle: Location
        for k in 0...3{
            Case_Actuelle = grille[c.x][k]
            if Case_Actuelle.est_occupee() {       // On regarde les types seulement si il ya une pièce sur la case
                if !(TypeDifferents.contains(Case_Actuelle.est_occupee_par()!.type)) {   // Si c'est un type qu'on a pas dans TypeDifferents
                    TypeDifferents.append(Case_Actuelle.est_occupee_par()!.type) // On l'y ajoute
                }
            }
        }
        return TypeDifferents.count == 4  // Si tous les types sont représentés sur la ligne
    }
    
    //colonne_complete: Plateau x Location -> Bool
    //renvoie si la colonne correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau
    func colonne_complete(_ c: Location) -> Bool {
        var TypeDifferents: [typePiece] = []
        var Case_Actuelle: Location

        for k in 0...3{
            Case_Actuelle=grille[k][c.y]
            if Case_Actuelle.est_occupee() {       // On regarde les types seulement si il ya une pièce sur la case
                if !(TypeDifferents.contains(Case_Actuelle.est_occupee_par()!.type)) {   // Si c'est un type qu'on a pas dans TypeDifferents
                    TypeDifferents.append(Case_Actuelle.est_occupee_par()!.type) // On l'y ajoute
                }
            }
        }
        return TypeDifferents.count == 4
    }
    
    //region_complete: Plateau x Location -> Bool
    //renvoie si la region correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau
    func region_complete(_ c: Location) -> Bool {
        var TypeDifferents: [typePiece] = []
        var Case_Actuelle: Location
        for l in (c.x/2)*2...(c.x/2)*2+1{  // On regarde les 2 lignes correspondant a la region
            for co in (c.y/2)*2...(c.y/2)*2+1 { // Idem avec les colonnes
                Case_Actuelle=grille[l][co]
                if Case_Actuelle.est_occupee() {       // On regarde les types seulement si il ya une piece sur la case
                    if !(TypeDifferents.contains(Case_Actuelle.est_occupee_par()!.type)) {   // Si c'est un type qu'on a pas dans TypeDifferents
                        TypeDifferents.append(Case_Actuelle.est_occupee_par()!.type) // On l'y ajoute
                    }
                }
            }
        }
        return TypeDifferents.count == 4
    }
   
    //pose_piece: Plateau x Joueur x Piece x Location -> Plateau x Bool
    //un joueur pose une pièce sur une case donnée du plateau
    //Pre: La case indiquée n'est pas déjà occupée
    //Pre: La case indiquée est "jouable", c'est à dire que on a le droit de poser la pièce dessus
    //Pre: La pièce n'est pas déjà posée ailleurs sur le plateau
    //Post: Si toutes les préconditions sont respectées alors la pièce du joueur est posée sur le Plateau et renvoie true sinon renvoie false

    // Piece_Posable : Fonction qui renvoie si il est possible dans le jeu de poser cette pièce ou non.
    func Piece_Posable(_ loc: Location, _ piece: Piece, _ joueur: Joueur) -> Bool {
        return !(loc.est_occupee() || piece.est_posee() || !self.coup_autorise(joueur, loc, piece))
    }
    
    func pose_piece(_ loc: Location, _ piece: Piece, _ joueur: Joueur) {
        if Piece_Posable(loc, piece, joueur) {
            //if (piece.pose_piece(coord)){
            //var coor : Location = loc
            loc.occupee = true
            loc.piece = piece
            piece.coord = loc
            //joueur.piece_sac.remove(at:piece.num-1)
            //piece.pose_piece(coor)
        }
    }
}


//affiche_grille: Plateau -> String
//renvoie la grille de jeu

func affiche_grille(plateau: Plateau)->String{
    var affiche : String = "                               x y 0  1  2  3\n                               0 |"
    for i in 0..<4{
        for j in 0..<4{
            if (plateau.grille[i][j].est_occupee()){
                if (plateau.grille[i][j].est_occupee_par()!.couleur.rawValue == "Blanc"){
                    if(plateau.grille[i][j].est_occupee_par()!.type.rawValue=="sphère"){affiche += " ● " //sphere
                    }
                    else if(plateau.grille[i][j].est_occupee_par()!.type.rawValue=="cube"){affiche += " ■ "//carre blanc
                    }
                    else if(plateau.grille[i][j].est_occupee_par()!.type.rawValue=="cylindre"){affiche += " ▮ "//cylindre 
                    }
                    else if(plateau.grille[i][j].est_occupee_par()!.type.rawValue=="cône"){affiche += " ▲ "//cone                                  
                    }
                }
                else if(plateau.grille[i][j].est_occupee_par()!.couleur.rawValue == "Noir"){//si piece noir
                            
                    if(plateau.grille[i][j].est_occupee_par()!.type.rawValue=="sphère"){affiche += " ○ " //sphere
                                
                    }else if(plateau.grille[i][j].est_occupee_par()!.type.rawValue=="cube"){affiche += " □ "//cube
                                                                             
                    }else if(plateau.grille[i][j].est_occupee_par()!.type.rawValue=="cylindre"){affiche += " ▯ "//cylindre
                                                                             
                    }else if(plateau.grille[i][j].est_occupee_par()!.type.rawValue=="cône"){affiche += " △ "//cone
                    }          
                }
            }else{affiche += " . "}  
        }
        if(i == 3){affiche += "| \n"} //Dernière ligne de l'affichage
        else{affiche += "| \n                               "+String(i+1)+" |"}
    }
    return affiche
}

var partie: Plateau = Plateau() //on crée un plateau de jeu

var J1: Joueur = partie.joueurs[0] //le j1 est le premier joueur de la liste de joueurs de la partie
var J2: Joueur = partie.joueurs[1] //le j2 est le second joeuer de la liste de joueurs de la partie
print("le joueur 1 joue les " + J1.couleur.rawValue) 
print("le joueur 2 joue les " + J2.couleur.rawValue)
var JoueurCourant: Joueur = J1 //le joueur 1 commence en premier
var AutreJoueur: Joueur = J2 //le second joueur jouera en deuxième
var pivot: Joueur //permet de permuter le joueur courant et l'autre joueur à la fin du tour du joueur courant

var bonCoup: Bool = false //permet à chaque tour de savoir si le joueur fait un bon coup

var bonIndice: Bool = false //permet de verifier qu'un joueur utilise bien une pièce qui existe et qui lui appartient

var bonCoord: Bool = false
while(partie.partie_en_cours == true){
    
    print("\n\u{1B}[93m -------------------------------------------\u{1B}[0m\n")
    if(partie.peut_jouer(JoueurCourant)){ //on verifie que le joueur à au moins une possibilité pour jouer
        bonCoup = false
        while(!bonCoup){ //tant que le joueur ne fait pas un bon coup, on repète la procédure
            print("Au tour du Joueur", JoueurCourant.couleur.rawValue,"\n")
            print("Plateau : ")
            print(affiche_grille(plateau: partie)) //affiche la grille
            print("Piece Joueur : " + JoueurCourant.couleur.rawValue)
            print(JoueurCourant.piece_Restantes()) //affiche les pieces restantes au joueur
            var piece1 : String = ""
            var piece2 : Int = -1
            bonIndice = false
            
            while(!bonIndice){ //on verifie que le joueur veut jouer une pièce existante
                print("Donnez le numéro de la pièce :")
                if let pieceStr = readLine(){piece1 = pieceStr}
                else{piece1 = "a"} //l'utilisateur saisit un nombre qui correspond à l'indice dans le sac de piece de la piece que l'on veut jouer
                if let pieceInt = Int(piece1){piece2 = pieceInt}
                else {piece2 = -1}
                
                if(piece2 < 8 && piece2 != -1){ //si l'on veut jouer une piece qui appartient à notre sac de piece
                    bonIndice = true //alors on peut passer à l'étape suivant (i.e. choisir l'endroit où l'on veut la mettre)
                }
            }   
            let piecejouee: Piece = JoueurCourant.piece_sac[piece2] //le joueur joue donc la piece qu'il a selectionné

            bonCoord = false
            var x : Int = -1
            var y : Int = -1

            while(!bonCoord){
                var xStr : String = "" 
                var yStr = ""
                
                print("coord X:")
                if let x1 = readLine(){xStr = x1} else{x = -1} // le joueur indique la ligne où il veut poser la pièce
                if let x2 = Int(xStr){x = x2} else{x = -1}
                print("coord Y:")
                if let y1 = readLine(){yStr = y1} else{y = -1} // le joueur indique la colonne où il veut poser la pièce
                if let y2 = Int(yStr){y = y2} else{y = -1}
                if(x >= 0 && y >= 0 && x < 4 && y < 4){bonCoord = true}
                else{print("\n\u{1B}[91mMauvaise coordonnées, recommencez svp (mettre sur une pièce pour changer de piece)\u{1B}[0m")} 
            }
            let coup: Location = partie.grille[x][y]
            if partie.coup_autorise(JoueurCourant, coup, piecejouee){
                if(partie.Piece_Posable(coup, piecejouee, JoueurCourant)){
                    partie.pose_piece(coup, piecejouee, JoueurCourant)
                    //si le coup respecte les règles alors le joueur pose la pièce
                    bonCoup = true
 //le coup a été effectué,on verifie si le joueur a gagné puis on passe au joueur suivant
                    if(partie.colonne_complete(coup) || partie.ligne_complete(coup) || partie.region_complete(coup)){ //si une ligne/colonne/region est totalement remplie alors le vainqueur est le joueur qui vient de poser la pièce
                        partie.partie_en_cours = false
                    }
                }
                else{print("\n\u{1B}[91mCoup sur pièce, recommencez svp\u{1B}[0m")} 
            }
            else{print("\n\u{1B}[91mCoup non-autorisé, recommencez svp\u{1B}[0m")} 
        }
    }
    else{ // si le 1er joueur ne peut pas jouer alors le second est le vainqueur
        partie.partie_en_cours = false
        pivot=JoueurCourant //on permute les joueurs car le joueur ne pouvant pas poser de pièce a perdu.
        JoueurCourant = AutreJoueur
        AutreJoueur = pivot
    }
    pivot=JoueurCourant //on permute les joueurs pour que le joueurCourant devienne le joueur qui ne joue pas
    JoueurCourant = AutreJoueur
    AutreJoueur = pivot
    bonCoup = false //on remet les variables à leurs valeurs de bases pour refaire les tests
    bonIndice = false
}

// Rajouté : Après victoire
print("\n\u{1B}[93m -------------------------------------------\u{1B}[0m\n")
print("\u{1B}[92mLE GAGNANT EST :\u{1B}[0m",AutreJoueur.couleur.rawValue)
print(affiche_grille(plateau: partie))
