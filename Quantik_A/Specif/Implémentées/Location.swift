class Location{
    public private(set) var x : Int
    public private(set) var y : Int
    private var occupee : Bool // à chaque fois qu'on pose une piece il faut qu'on set cette var à True et il faut regarder si cette var est à False
    private var piece : Piece?

    
    init(x: Int, y: Int){
        self.x = x
        self.y = y
        self.occupee = false
        self.piece = nil
    }

    func est_occupee()->Bool{
        return occupee
    }

    func est_occupee_par()->Piece?{
        if self.est_occupee(){
            return self.piece
        }else{
            return nil
        }
    }
}
