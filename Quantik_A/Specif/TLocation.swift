public protocol TLocation{

    //x: Location -> Int
    //contient la coordonnee (ligne de la case)
    //x est compris entre 0 et 3
    var x:Int{get}

    //y: Location -> Int
    //contient la coordonnee (colonne de la case)
    //y est compris entre 0 et 3
    var y:Int{get}

    //init: Int x Int -> Location
    //initialise les coordonnees de la case
    //Pre: x et y sont tous les deux compris entre 0 et 3
    init(_ x:Int,_ y:Int)

    //est_occupee: Location -> Bool
    //renvoie si la case est occupée par une pièce ou non.
    //Pre: La case est déjà créee
    func est_occupee() -> Bool

    
    //est_occupee_par: Location -> Piece | Vide
    //si une piece occupe la case, renvoie cette piece sinon renvoie Vide
    //Post: La piece retournée existe 
    func est_occupee_par() -> TPiece?
    
}
