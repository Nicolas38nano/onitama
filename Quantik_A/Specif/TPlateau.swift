public protocol TPlateau{

    
    //init : -> Plateau
    //initialise le plateau de jeu (un plateau de jeu de 4 cases sur 4 cases)
    //crée deux joueurs de couleurs différentes (noir ou blanc)
    //chaque joueur possède 8 pièces de sa couleur (2 Cylindres, 2 Cubes, 2 Sphères, 2 Pyramides)
    //Post: les cases du plateau ne sont pas occupées à la création
    //Post: partie_en_cours(init())=true
    init()

    //partie_en_cours: Plateau -> Bool
    //stocke si la partie est en cours, dans le cas contraire un joueur a remporter la partie
    //Lorsque qu'on crée la partie, la partie est en cours
    //Pre: La partie doit avoir été créee
    var partie_en_cours:Bool{get set}

    //joueurs: Plateau -> [Joueur]
    //renvoie la liste des joueurs
    //Pre: La partie doit avoir été créee
    var joueurs:[TJoueur]{get}

    //peut_jouer: Plateau x Joueur -> Bool
    //le joueur peut-il jouer c'est à dire avoir au moins une possibilité pour poser une de ses pièces restantes, si oui la partie continue, sinon l'autre joueur gagne
    //Pre: Le plateau de jeu a été initialisé
    //Pre: La partie est en cours
    //Pre: Le joueur a été initialisé
    func peut_jouer(_ j:TJoueur) -> Bool

    //coup_autorise: Plateau x Joueur x Location -> Bool
    //renvoie si le coup tenté par le joueur est réglementaire
    //func coup_autorise(_ j:Joueur,_ c:Location)

    //colonne_occupee: Plateau x Piece x Int -> Bool
    //renvoie "true" si la piece peut être placée dans la colonne
    //Pre: "c" est compris entre 0 et 3
    //Pre: La pièce "p" a été créee mais n'a pas déjà été posée
    func colonne_occupee(_ p:TPiece,_ c:Int) -> Bool

    //ligne_occupee: Plateau x Piece x Int -> Bool
    //renvoie "true" si la pièce peut être placée dans la ligne
    //Pre: "l" est compris entre 0 et 3
    //Pre: La pièce "p" a été créee mais n'a pas déjà été posée
    func ligne_occupee(_ p:TPiece,_ l:Int) -> Bool

    //region_occupee: Plateau x Piece x Location -> Bool
    //renvoie "true" si la piece peut être posée dans la région
    //Pre: c.x compris entre 0 et 3 et c.y compris entre 0 et 3
    //Pre: La case saisie est initialisée et dans le plateau
    //Pre: La pièce "p" a été créee mais n'a pas déjà été posée
    func region_occupee(_ p:TPiece,_ c:TLocation) -> Bool
    
    //ligne_complete: Plateau x Location -> Bool
    //renvoie si la ligne correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau
    func ligne_complete(_ c:TLocation) -> Bool
    
    //colonne_complete: Plateau x Location -> Bool
    //renvoie si la colonne correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau
    func colonne_complete(_ c:TLocation) -> Bool
    
    //region_complete: Plateau x Location -> Bool
    //renvoie si la region correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau
    func region_complete(_ c:TLocation) -> Bool
    
    //affiche_grille: Plateau -> String
    //renvoie la grille de jeu
    func affiche_grille() -> String
    
    
}
