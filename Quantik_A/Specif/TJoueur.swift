public protocol TJoueur{

    
    //sacPiece: Joueur -> [Piece]
    //donne la liste des pièces que possède le joueurs
    //var sacPiece:[TPiece] {get}

    //couleur: Joueur -> Couleur
    //donne la couleur du joueur conformement au type Couleur
    var couleur:Couleur {get}

    //init :  Couleur -> Joueur
    //initialise un joueur qui dispose de 8 pièces de même couleur que la sienne
    //les pieces sont les suivantes: 2 cylindres, 2 pyramides, 2 sphères, 2 cubes
    //Pre: La couleur du joueur (et donc de ses pièces) est soit noir ou soit blanc
    //Post: couleur(joueur)==couleur(piece(joueur))
    //Post: Taille du sacPiece = 8
    //Post: type(SacPiece[0])=type(SacPiece[1]) = cylindre
    //Post: type(SacPiece[2])=type(SacPiece[3]) = pyramide
    //Post: type(SacPiece[4])=type(SacPiece[5]) = sphère
    //Post: type(SacPiece[6])=type(SacPiece[7]) = cube
     init(_ c:Couleur)

    
    //pose_piece: Plateau x Joueur x Piece x Location -> Plateau
    //un joueur pose une pièce sur une case donnée du plateau
    //Pre: La case indiquée n'est pas déjà occupée
    //Pre: La case indiquée est "jouable", c'est à dire que on a le droit de poser la pièce dessus
    //Pre: La pièce n'est pas déjà posée ailleurs sur le plateau
    //Post: Si toutes les préconditions sont respectées alors la pièce du joueur est posée sur le Plateau
     mutating func pose_piece(_ pl:TPlateau,_ p:TPiece,_ l:TLocation) -> TPlateau

    //pieceRestantes: Joueur -> Int
    //donne le type et la position dans le sac de chaque pieces restantes au joueur (i.e. celles qui ne sont pas posées)
    func piece_Restantes() -> String
}
