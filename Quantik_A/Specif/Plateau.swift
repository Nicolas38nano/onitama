struct Plateau{

        
     //init : -> Plateau
    //initialise le plateau de jeu (un plateau de jeu de 4 cases sur 4 cases)
    //crée deux joueurs de couleurs différentes (noir ou blanc)
    //chaque joueur possède 8 pièces de sa couleur (2 Cylindres, 2 Cubes, 2 Sphères, 2 Pyramides)
    //Post: les cases du plateau ne sont pas occupées à la création
    //Post: partie_en_cours(init())=true
    init(){
        for i in 0...3{
            for j in 0...3{
                self.grille[j][i] = Location(x: i, y: j)
            }
        }
        var random : Int = Int.random(in: 0...1)
        self.joueurs[random] = Joueur("Blanc")
        if (random == 1){
            self.joueur[0] = Joueur("Noir")
        }else{self.joueur[1] = Joueur("Noir")}

        self.partie_en_cours = True
    }


    // AJOUTEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
    private(set) var grille:[[Location]] {return grille}
    
    //partie_en_cours: Plateau -> Bool
    //stocke si la partie est en cours, dans le cas contraire un joueur a remporter la partie
    //Lorsque qu'on crée la partie, la partie est en cours
    //Pre: La partie doit avoir été créee
    var partie_en_cours:Bool

    //joueurs: Plateau -> [Joueur]
    //renvoie la liste des joueurs
    //Pre: La partie doit avoir été créee
    var joueurs:[TJoueur]{get}

    //peut_jouer: Plateau x Joueur -> Bool
    //le joueur peut-il jouer c'est à dire avoir au moins une possibilité pour poser une de ses pièces restantes, si oui la partie continue, sinon l'autre joueur gagne
    //Pre: Le plateau de jeu a été initialisé
    //Pre: La partie est en cours
    //Pre: Le joueur a été initialisé
    func peut_jouer(_ j:TJoueur) -> Bool 
        

    //coup_autorise: Plateau x Joueur x Location -> Bool
    //renvoie si le coup tente par le joueur est reglementaire
    func coup_autorise(_ j:Joueur,_ c:Location) -> Bool

    //colonne_occupee: Plateau x Piece x Int -> Bool
    //renvoie "true" si la piece peut être placée dans la colonne
    //Pre: "c" est compris entre 0 et 3
    //Pre: La piece "p" a été creee mais n'a pas déjà été posee
    func colonne_occupee(_ p:TPiece,_ c:Int) -> Bool {                     //Pas besoin de checker si la case est occupee ou non
        var Case_Actuelle:Location //Variable de lisibilite
        var PasPossible:Bool=False
        for k in 0...3{  //On regarde les 4 lignes correspondant a la colonne c
            Case_Actuelle=self.grille[k][c]
            if  Case_Actuelle.est_occupee() {// Pour chacune de ces lignes, on regarde si la case correspondante est occupee par une piece
                if Case_Actuelle.est_occupee_par.couleur()!=JoueurCourant.couleur() and Case_Actuelle.est_occupee_par.type=p.type(){  //Si une piece adverse de meme type est presente sur la case
                    PasPossible=True
                }
            }
        }
        return PasPossible
    }
            
            

    //ligne_occupee: Plateau x Piece x Int -> Bool
    //renvoie "true" si la pièce peut être placée dans la ligne
    //Pre: "l" est compris entre 0 et 3
    //Pre: La pièce "p" a été créee mais n'a pas déjà été posée
    func ligne_occupee(_ p:TPiece,_ l:Int) -> Bool {
        var Case_Actuelle:Location //Variable de lisibilite
        var PasPossible:Bool=False
        for k in 0...3{  //On regarde les 4 colonnes correspondant a la ligne l
            Case_Actuelle=self.grille[l][k]
            if  Case_Actuelle.est_occupee() {// Pour chacune de ces colonnes, on regarde si la case correspondante est occupee par une piece
                if Case_Actuelle.est_occupee_par.couleur()!=JoueurCourant.couleur() and Case_Actuelle.est_occupee_par.type=p.type(){  //Si une piece adverse de meme type est presente sur la case
                    PasPossible=True
                }
            }
        }
        return PasPossible
    }

    //region_occupee: Plateau x Piece x Location -> Bool
    //renvoie "true" si la piece peut être posée dans la région
    //Pre: c.x compris entre 0 et 3 et c.y compris entre 0 et 3
    //Pre: La case saisie est initialisée et dans le plateau
    //Pre: La pièce "p" a été créee mais n'a pas déjà été posée
    func region_occupee(_ p:TPiece,_ c:TLocation) -> Bool {
        var Case_Actuelle:Location //Indice
        var PasPossible:Bool=False
        for l in (c.x/2)*2...(c.x/2)*2+1{  // On regarde les 2 lignes correspondant a la region
            for c in (c.y/2)*2...(c.y/2)*2+1 { // Idem avec les colonnes
                Case_Actuelle=self.grille[l][c]
                if  Case_Actuelle.est_occupee() {// Pour chacune de ces cases, on regarde si la case correspondante est occupee par une piece
                    if Case_Actuelle.est_occupee_par.couleur()!=JoueurCourant.couleur() and Case_Actuelle.est_occupee_par.type=p.type(){  //Si une piece adverse de meme type est presente sur la case
                        PasPossible=True
                    }
                }
            }
        }
        return PasPossible
    }


   
    
    //ligne_complete: Plateau x Location -> Bool
    //renvoie si la ligne correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau
    func ligne_complete(_ c:TLocation) -> Bool {                 //UNE LIGNE PEU ETRE PLEINE MAIS PAS COMPLETE (si un joueur pose 2 fois la meme piece)
        var TypeDifferents:(typepiece)
        var Case_Actuelle:Location
        for k in 0...3{
            Case_Actuelle=grille[c.x][k]
            if Case_Actuelle.est_occupee(){       // On regarde les types seulement si il ya une pièce sur la case
                if !(Case_Actuelle.est_occupee_par.type() in TypeDifferents){   // Si c'est un type qu'on a pas dans TypeDifferents
                    TypeDifferents.append(Case_Actuelle.est_occupee_par.type()) // On l'y ajoute
                }
            }
        }
        if len(TypeDifferents)==4{  // Si tous les types sont représentés sur la ligne
            return true
        }
        return false
    }
    
    //colonne_complete: Plateau x Location -> Bool
    //renvoie si la colonne correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau
    func colonne_complete(_ c:TLocation) -> Bool {
        var TypeDifferents:(typepiece)
        var Case_Actuelle:Location
        for k in 0...3{
            Case_Actuelle=grille[k][c.y]
            if Case_Actuelle.est_occupee(){       // On regarde les types seulement si il ya une pièce sur la case
                if !(Case_Actuelle.est_occupee_par.type() in TypeDifferents){   // Si c'est un type qu'on a pas dans TypeDifferents
                    TypeDifferents.append(Case_Actuelle.est_occupee_par.type()) // On l'y ajoute
                }
            }
        }
        if len(TypeDifferents)==4{  // Si tous les types sont représentés sur la colonne
            return true
        }
        return false
    }
    
    //region_complete: Plateau x Location -> Bool
    //renvoie si la region correspondante à la case est complete (le cas échant un joueur à donc remporter la partie)
    //Pre: la case saisie a été initialisée et est dans le plateau
    func region_complete(_ c:TLocation) -> Bool {
        var TypeDifferents:(typepiece)
        var Case_Actuelle:Location
        for l in (c.x/2)*2...(c.x/2)*2+1{  // On regarde les 2 lignes correspondant a la region
            for co in (c.y/2)*2...(c.y/2)*2+1 { // Idem avec les colonnes
                Case_Actuelle=grille[l][co]
                if Case_Actuelle.est_occupee(){       // On regarde les types seulement si il ya une piece sur la case
                    if !(Case_Actuelle.est_occupee_par.type() in TypeDifferents){   // Si c'est un type qu'on a pas dans TypeDifferents
                        TypeDifferents.append(Case_Actuelle.est_occupee_par.type()) // On l'y ajoute
                }
            }
        }
        if len(TypeDifferents)==4{  // Si tous les types sont représentés sur la region
            return true
        }
        return false
    }
    
    //affiche_grille: Plateau -> String
    //renvoie la grille de jeu
    func affiche_grille() -> String

    //pose_piece: Plateau x Joueur x Piece x Location -> Plateau
    //un joueur pose une pièce sur une case donnée du plateau
    //Pre: La case indiquée n'est pas déjà occupée
    //Pre: La case indiquée est "jouable", c'est à dire que on a le droit de poser la pièce dessus
    //Pre: La pièce n'est pas déjà posée ailleurs sur le plateau
    //Post: Si toutes les préconditions sont respectées alors la pièce du joueur est posée sur le Plateau
     mutating func pose_piece(_ pl:TPlateau,_ p:TPiece,_ l:TLocation) -> TPlateau    
    




}