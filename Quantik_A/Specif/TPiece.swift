public protocol TPiece{

    //couleur: Piece -> Couleur
    //stocke la couleur de la pièce (soit noir soit blanc)
    //Pre: La pièce a été initialisée
    var couleur:Couleur{get}
    
    //type: Piece -> TypePiece
    //stocke le type de la pièce (conformement au type typePiece)
    var type:typepiece{get}

    //coord: Piece -> Location | Vide
    //stocke les coordonnees de la piece si elle est placée sinon Vide
    var coord:TLocation?{get set}

    //init Couleur x typePiece -> Piece
    //initialise une piece de couleur et de type donné dont les coordonnées sont Vide (i.e. la pièce n'est pas placée)
    //Pre: La couleur de la pièce est Noir ou Blanc (conformement au type Couleur)
    //Pre: Le type de la pièce est Cube ou Cylindre ou Sphere ou Pyramide (conformement au type typePiece)
    //Post: coord(init())=Vide
    init(_ c:Couleur,_ t:typepiece)

    //est_posee: Piece -> Bool
    //retourne si la pièce est posée, c'est à dire que les coordonnées ne sont pas vides
    //Pre: La pièce a été créee
    func est_posee() -> Bool

    //est_posee_sur: Piece -> Location | Vide
    //retourne sur quelle case la piece est posée, si elle n'est pas posée alors retourne Vide
    //Pre: La pièce a été créee
    //Post: La case retournée existe
    func est_posee_sur()->TLocation?
    

    
    
    
}
