
var partie:TPlateau //on crée un plateau de jeu
var J1:TJoueur = partie.joueurs[0] //le j1 est le premier joueur de la liste de joueurs de la partie
var J2:TJoueur = partie.joueurs[1] //le j2 est le second joeuer de la liste de joueurs de la partie
print("le joueur 1 joue les " + J1.couleur) 
print("le joueur 2 joue les " + J2.couleur) 
var JoueurCourant:TJoueur=J1 //le joueur 1 commence en premier
var AutreJoueur:TJoueur=J2 //le second joueur jouera en deuxième
var pivot:TJoueur //permet de permuter le joueur courant et l'autre joueur à la fin du tour du joueur courant
var bonCoup:Bool=false //permet à chaque tour de savoir si le joueur fait un bon coup
var bonIndice:Bool=false //permet de verifier qu'un joueur utilise bien une pièce qui existe et qui lui appartient

while(partie.partie_en_cours == true){
    
    
    if(partie.peut_jouer(JoueurCourant)){ //on verifie que le joueur à au moins une possibilité pour jouer
        while(bonCoup==false){ //tant que le joueur ne fait pas un bon coup, on repète la procédure
            print("au tour du Joueur courant, choisir la piece à jouer puis l'endroit où l'on veut poser la pièce mais avant voici vos pièces restantes: ")
            print(partie.affiche_grille()) //affiche la grille
            print(JoueurCourant.piece_Restantes()) //affiche les pieces restantes au joueur
            while(bonIndice==false){ //on verifie que le joueur veut jouer une pièce existante
                guard let piece1=readLine() else{fatalError("erreur de saisie")} //l'utilisateur saisit un nombre qui correspond à l'indice dans le sac de piece de la piece que l'on veut jouer
                guard let piece2=int(piece1) else {fatalError("erreur de saisie")}
                if(piece2<8){ //si l'on veut jouer une piece qui appartient à notre sac de piece
                    bonIndice=true //alors on peut passer à l'étape suivant (i.e. choisir l'endroit où l'on veut la mettre)
                }
            }   
            var piecejouee:TPiece=JoueurCourant.SacPiece[piece2] //le joueur joue donc la piece qu'il a selectionné 
            guard let x1=readLine() else{fatalError("erreur de saisie")} // le joueur indique la ligne où il veut poser la pièce
            guard let x2=int(x1) else{fatalError("erreur de saisie")}
            guard let y1=readLine() else{fatalError("erreur de saisie")} // le joueur indique la colonne où il veut poser la pièce
            guard let y2=int(y1) else{fatalError("erreur de saisie")}
            var coup:TLocation=TLocation(x2,y2)
            if (!partie.colonne_occupee(piecejouee,coup.y) && !partie.ligne_occupee(piecejouee,coup.x) && !partie.region_occupee(piecejouee,coup) && !coup.estoccupee() && !piecejouee.est_posee()){
                JoueurCourant.pose_piece(partie,piecejouee,coup) //si le coup respecte les règles alors le joueur pose la pièce
                print("le joueur courant à joué")
                bonCoup=True //le coup a été effectué,on verifie si le joueur à gagner puis on passe au joueur suivant
                if(partie.colonne_remplie(coup) || partie.ligne_remplie(coup) || partie.region_remplie(coup)){ //si une ligne/colonne/region est totalement remplie alors le vainqueur est le joueur qui vient de poser la pièce
                    partie.partie_en_cours=false
                }
            }
        }
    }
    else{ // si le 1er joueur ne peut pas jouer alors le second est le vainqueur
        print("Autre joueur gagne")
        partie.partie_en_cours=false
    }
    pivot=JoueurCourant //on permute les joueurs pour que le joueurCourant devienne le joueur qui ne joue pas
    JoueurCourant=AutreJoueur
    AutreJoueur=pivot
    bonCoup=false //on remet les variables à leurs valeurs de bases pour refaire les tests
    bonIndice=false
  }
  
}

